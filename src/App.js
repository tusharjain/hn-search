import './App.css';
import Navbar from './components/Navbar';
import Newsboard from './components/Newsboard';
import SearchFilter from './components/SearchFilter';
import Footer  from './components/Footer';

function App() {
  return (
    <div className="App">
      <Navbar />
      <SearchFilter />
      <Newsboard />
      <Footer />
    </div>
  );
}

export default App;
