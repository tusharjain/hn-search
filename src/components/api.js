const getTopStories = async (setTopStories) => {
  try {
    await fetch(
      " https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty"
    )
      .then((data) => data.json())
      .then((data) => {
        let dataSlice = data.slice(0, 20);
        let temptopStories = [];
        for (let id of dataSlice) {
          try {
            fetch(
              `https://hacker-news.firebaseio.com/v0/item/${id}.json?print=pretty`
            )
              .then((data) => data.json())
              .then((data) => {
                
                temptopStories.push(data);
                setTopStories(temptopStories);
              });
          } catch (error) {
            console.log(error);
          }
        }
        return temptopStories;
      })
      
  } catch (error) {
    console.log(error);
  }
};

export { getTopStories };
