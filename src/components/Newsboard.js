import React from "react";

import NewsItem from "./NewsItem";
// import RenderNewsStories from './RenderNewsStories';

import { getTopStories } from "./api";

export default class Newsboard extends React.Component {
  constructor() {
    super();
    this.state = {
      topStories: [],
    };
    this.setTopStories = this.setTopStories.bind(this);
  }

  setTopStories(arr) {
    this.setState({ topStories: arr });
  }

  componentDidMount() {
    getTopStories(this.setTopStories);
  }

  render() {
    let topStoriesTags;
    if (this.state.topStories.length) {
      console.log("*");
      topStoriesTags = this.state.topStories.map((story) => (
        <NewsItem story={story} key={story.id} />
      ));
    }

    return <div className="newsboard">{topStoriesTags}</div>;
  }
}
