import React from "react";

export default function NewsItem(props) {
  console.log(props.story);

  return (
    <article className="article">
      <div class="story_data">
        <div class="story_title">
          <a href={`https://news.ycombinator.com/item?id=${props.story.id}`}>
            <span>{props.story.title}</span>
          </a>
        </div>
        <div class="story_link">
          <a href={props.story.url}>
            <span>({props.story.url})</span>
          </a>
        </div>
      </div>
      <div class="story_meta">
        <span>{props.story.score} points</span> |&nbsp;<span>{props.story.by}</span> |&nbsp;
        <span>{props.story.time}</span> |&nbsp;
        <span>{props.story.descendants} comments</span>
      </div>
    </article>
  );
}
